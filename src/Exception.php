<?php

declare(strict_types=1);

namespace ZdenekGebauer\Template;

class Exception extends \Exception
{
    final public const TAG_NOT_FOUND = 1,
        SUB_TEMPLATE_NOT_FOUND = 2,
        SUB_TEMPLATE_NOT_UNIQUE = 3;
}
