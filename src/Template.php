<?php

declare(strict_types=1);

namespace ZdenekGebauer\Template;

/**
 * simple string based template engine
 *
 * Templates can be read from files.
 * Expect placeholders surrounded by [%...%]
 * and "sub templates" surrounded by '<!--<SUB_some_template>--> ... <!--</SUB_some_template>-->
 */
class Template
{
    protected string $content = '';

    protected string $file = '';

    /**
     * create instance and fill content from given file
     *
     * @param string $fullPath fill content from this file
     * @param bool $useBody true|false = read only part between tags <body>|whole file
     */
    public function __construct(string $fullPath = '', bool $useBody = true)
    {
        if ($fullPath !== '') {
            $this->content = (string)file_get_contents($fullPath);
            $this->file = $fullPath;
            if (
                $useBody
                && $this->content !== ''
                && preg_match('/<body[^>]*>(.*?)<\\/body>/is', $this->content, $matches) === 1
            ) {
                $this->content = $matches[1];
            }
        }
    }

    public function get(): string
    {
        return $this->content;
    }

    /**
     * @param array<string, float|int|string> $replacements
     * @param bool $quiet true = ignore missing tag
     * @throws Exception
     */
    public function replaceArray(array $replacements, bool $quiet = false): void
    {
        foreach ($replacements as $tag => $value) {
            $this->replace($tag, $value, $quiet);
        }
    }

    /**
     * replace tag with specified value with converted html entities
     *
     * example:
     * <code>
     * $template->set('lorem [%TXT_TAG%] ipsum';
     * $template->replace('TAG', '<p>VALUE</p>');
     * $template->get(); // returns lorem &lt;p&gt;VALUE&lt;/p&gt; ipsum
     * </code>
     *
     * @param bool $quiet true = ignore missing tag
     * @throws Exception
     */
    public function replace(string $tag, bool|int|float|string|null $value, bool $quiet = false): void
    {
        $this->replaceTag($tag, htmlspecialchars((string)$value, ENT_QUOTES), $quiet);
    }

    /**
     * replace tag (for example [%TXT_VALUE%]) with specified value
     *
     * @param string $tag tag without delimiters
     * @param bool $quiet true = ignore missing tag
     * @throws Exception
     */
    protected function replaceTag(string $tag, string $value, bool $quiet): void
    {
        $count = 0;
        $this->content = str_replace($this->getPseudoTag($tag), $value, $this->content, $count);
        if ($count === 0 && !$quiet) {
            throw new Exception('tag "' . $tag . '" not found in file ' . $this->file, Exception::TAG_NOT_FOUND);
        }
    }

    protected function getPseudoTag(string $tag): string
    {
        return '[%' . $tag . '%]';
    }

    /**
     * replace tag with specified value without converting html entities
     *
     * example:
     * <code>
     * $template->set('lorem [%TXT_TAG%] ipsum';
     * $template->replace('TAG', '<p>VALUE</p>');
     * $template->get(); // returns lorem <p>VALUE</p> ipsum
     * </code>
     *
     * @param bool $quiet true = ignore missing tag
     * @throws Exception
     */
    public function replaceRaw(string $tag, string $value, bool $quiet = false): void
    {
        $this->replaceTag($tag, $value, $quiet);
    }

    /**
     * Replace all occurrences of the search string with the replacement string
     */
    public function replaceString(string $search, string|int|float $replace): void
    {
        $this->content = str_replace($search, (string)$replace, $this->content);
    }

    public function set(string $value): void
    {
        $this->content = $value;
    }

    /**
     * strip or delete sub template
     *
     * @param bool $condition TRUE|FALSE = strip|delete sub template
     * @throws Exception
     */
    public function showSub(string $tag, bool $condition): void
    {
        if ($condition) {
            $this->stripSub($tag);
        } else {
            $this->delSub($tag);
        }
    }

    /**
     * replace sub template (for example <!-- <SUB_PART> -->) with content of sub template itself
     *
     * @param string $tag sub template tag without delimiters
     * @throws Exception
     */
    public function stripSub(string $tag): void
    {
        $this->replaceSub($tag, $this->getSub($tag));
    }

    /**
     * replace sub template (for example <!-- <SUB_PART> -->) with given value
     *
     * @param string $tag sub template tag without delimiters
     * @throws Exception
     */
    public function replaceSub(string $tag, string $value): void
    {
        $count = 0;
        $this->content = (string)preg_replace($this->getRegexp($tag), $value, $this->content, -1, $count);
        if ($count !== 1) {
            throw new Exception(
                'expected 1 occurrence of subtemplate "' . $tag . '", found ' . $count . ' in file ' . $this->file,
                Exception::SUB_TEMPLATE_NOT_UNIQUE
            );
        }
    }

    /**
     * returns regular expression for sub template
     *
     * Group with content of subtemplate is named "sub_template"
     *
     * @param string $tag sub template description without delimiters
     */
    protected function getRegexp(string $tag): string
    {
        return '/<!--(\s*)<SUB_' . $tag . '>(\s*)-->(?<sub_template>.*)<!--(\s*)<\/SUB_' . $tag . '>(\s*)-->/Usm';
    }

    /**
     * returns content of sub template
     *
     * example:
     * <code>
     * $template->set('lorem <!--<SUB_TEMPLATE>-->sub template<!--</SUB_TEMPLATE>--> ipsum';
     * $template->getSub('TEMPLATE'); // returns 'sub template'
     * </code>
     *
     * @param string $tag sub template tag without delimiters
     * @throws Exception
     */
    public function getSub(string $tag): string
    {
        $ret = [];
        if (preg_match($this->getRegexp($tag), $this->content, $ret) === 1) {
            return $ret['sub_template'];
        }
        throw new Exception(
            'sub template "' . $tag . '" not found in file ' . $this->file,
            Exception::SUB_TEMPLATE_NOT_FOUND
        );
    }

    /**
     * remove entire sub template (for example <!-- <SUB_PART> -->content<!-- </SUB_PART> -->)
     *
     * @param string $tag sub template description without delimiters
     * @throws Exception
     */
    public function delSub(string $tag): void
    {
        $this->replaceSub($tag, '');
    }
}
