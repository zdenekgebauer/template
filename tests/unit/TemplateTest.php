<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Exception\Warning;
use Codeception\Test\Unit;
use Tests\Support\UnitTester;
use ZdenekGebauer\Template\Exception;
use ZdenekGebauer\Template\Template;

class TemplateTest extends Unit
{
    protected UnitTester $tester;

    private string|bool $file = '';

    private string $content = '';

    public function testConstructor(): void
    {
        $obj = new Template();
        $this->tester->assertEquals('', $obj->get());
    }

    public function testConstructorEntireFile(): void
    {
        $obj = new Template($this->file, false);
        $this->tester->assertEquals($this->content, $obj->get());
    }

//    public function testConstructorFail(): void
//    {
//        $this->tester->expectThrowable(new Warning('aa', 0 , 'Temnplate.php', 11), static function () {
//            new Template('file_not_exists');
//        });
//
////        $this->expectException(Exception::class);
////        $this->expectExceptionMessage(
////            'file_get_contents(file_not_exists): failed to open stream: No such file or directory'
////        );
////        new Template('file_not_exists');
//    }

    public function testConstructorOnlyBody(): void
    {
        $obj = new Template($this->file);
        $expect = '[%VALUE%]<!-- <SUB_TEMPLATE> -->content of subtemplate<!-- </SUB_TEMPLATE> -->';
        $this->tester->assertEquals($expect, $obj->get());
    }

    public function testDelSub(): void
    {
        $obj = new Template();
        $obj->set($this->content);
        $obj->delSub('TEMPLATE');
        $this->tester->assertEquals('<body>[%VALUE%]</body>', $obj->get());
    }

    public function testDelSubTagNotExists(): void
    {
        $obj = new Template();

        $this->tester->expectThrowable(
            new Exception(
                'expected 1 occurrence of subtemplate "TEMPLATE_NOT_EXISTS", found 0 in file ',
                Exception::SUB_TEMPLATE_NOT_UNIQUE
            ),
            static function () use ($obj) {
                $obj->delSub('TEMPLATE_NOT_EXISTS');
            }
        );
    }

    public function testGetSub(): void
    {
        $obj = new Template();
        $obj->set($this->content);
        $this->tester->assertEquals('content of subtemplate', $obj->getSub('TEMPLATE'));
    }

    public function testGetSubTagNotExists(): void
    {
        $obj = new Template();

        $this->tester->expectThrowable(
            new Exception('sub template "TEMPLATE_NOT_EXISTS" not found in file ', Exception::SUB_TEMPLATE_NOT_FOUND),
            static function () use ($obj) {
                $obj->getSub('TEMPLATE_NOT_EXISTS');
            }
        );
    }

    public function testReplace(): void
    {
        $obj = new Template($this->file);
        $obj->replace('VALUE', 'replaced');
        $expect = 'replaced<!-- <SUB_TEMPLATE> -->content of subtemplate<!-- </SUB_TEMPLATE> -->';
        $this->tester->assertEquals($expect, $obj->get());
    }

    public function testReplaceArray(): void
    {
        $obj = new Template();
        $obj->set('<p>lorem [%TAG1%] ipsum  [%TAG2%] dolor</p>');

        $obj->replaceArray([
                               'TAG1' => 'VALUE 1',
                               'TAG2' => 'VALUE 2',
                               'TAG_NOT_EXISTS' => 'VALUE 3',
                           ], true);
        $expect = '<p>lorem VALUE 1 ipsum  VALUE 2 dolor</p>';
        $this->tester->assertEquals($expect, $obj->get());
    }

    public function testReplaceArrayWithInvalidTag(): void
    {
        $obj = new Template();
        $obj->set('<p>lorem [%TAG1%] ipsum  [%TAG2%] dolor</p>');

        $this->tester->expectThrowable(
            new Exception('tag "TAG_NOT_EXISTS" not found in file ', Exception::TAG_NOT_FOUND),
            static function () use ($obj) {
                $obj->replaceArray([
                                       'TAG1' => 'VALUE 1',
                                       'TAG2' => 'VALUE 2',
                                       'TAG_NOT_EXISTS' => 'VALUE 3',
                                   ]);
                $expect = '<p>lorem VALUE 1 ipsum  VALUE 2 dolor</p>';
                $this->tester->assertEquals($expect, $obj->get());
            }
        );
    }

    public function testReplaceHtml(): void
    {
        $obj = new Template($this->file);
        $obj->replace('VALUE', '<p>replaced</p>');
        $expect = '&lt;p&gt;replaced&lt;/p&gt;<!-- <SUB_TEMPLATE> -->content of subtemplate<!-- </SUB_TEMPLATE> -->';
        $this->tester->assertEquals($expect, $obj->get());
    }

    public function testReplaceNotExistsTag(): void
    {
        $obj = new Template($this->file);
        $this->tester->expectThrowable(
            new Exception('tag "VALUE_NOT_EXISTS" not found in file ' . $this->file, Exception::TAG_NOT_FOUND),
            static function () use ($obj) {
                $obj->replace('VALUE_NOT_EXISTS', 'replaced');
            }
        );
    }

    public function testReplaceRaw(): void
    {
        $obj = new Template($this->file);
        $obj->replaceRaw('VALUE', '<p>replaced</p>');
        $expect = '<p>replaced</p><!-- <SUB_TEMPLATE> -->content of subtemplate<!-- </SUB_TEMPLATE> -->';
        $this->tester->assertEquals($expect, $obj->get());
    }

    public function testReplaceString(): void
    {
        $obj = new Template();
        $obj->set('<p>lorem ipsum </p><p>lorem ipsum </p>');

        $obj->replaceString('lorem', 'dolor');
        $expect = '<p>dolor ipsum </p><p>dolor ipsum </p>';
        $this->tester->assertEquals($expect, $obj->get());
    }

    public function testReplaceSub(): void
    {
        $obj = new Template();
        $obj->set($this->content);
        $obj->replaceSub('TEMPLATE', 'replaced content');
        $expect = '<body>[%VALUE%]replaced content</body>';
        $this->tester->assertEquals($expect, $obj->get());
    }

    public function testReplaceSubMultipleTags(): void
    {
        $obj = new Template();

        $obj->set(
            '<body>[%VALUE%]<!-- <SUB_TEMPLATE> -->content of subtemplate<!-- </SUB_TEMPLATE> --> <!-- <SUB_TEMPLATE> -->content of another subtemplate<!-- </SUB_TEMPLATE> --></body>'
        );

        $this->tester->expectThrowable(
            new Exception('expected 1 occurrence of subtemplate "TEMPLATE", found 2 in file ', Exception::SUB_TEMPLATE_NOT_UNIQUE),
            static function () use ($obj) {
                $obj->replaceSub('TEMPLATE', 'replaced content');
            }
        );
    }

    public function testReplaceSubTagNotExists(): void
    {
        $obj = new Template();
        $obj->set($this->content);

        $this->tester->expectThrowable(
            new Exception(
                'expected 1 occurrence of subtemplate "TEMPLATE_NOT_EXISTS", found 0 in file ',
                Exception::SUB_TEMPLATE_NOT_UNIQUE
            ),
            static function () use ($obj) {
                $obj->replaceSub('TEMPLATE_NOT_EXISTS', 'replaced content');
            }
        );
    }

    public function testSet(): void
    {
        $content = 'lorem ipsum dolor';
        $obj = new Template();
        $obj->set($content);
        $this->tester->assertEquals($content, $obj->get());
    }

    public function testShowSubFalse(): void
    {
        $obj = new Template();
        $obj->set($this->content);
        $obj->showSub('TEMPLATE', false);
        $this->tester->assertEquals('<body>[%VALUE%]</body>', $obj->get());
    }

    public function testShowSubTagNotExists(): void
    {
        $obj = new Template();

        $this->tester->expectThrowable(
            new Exception('sub template "TEMPLATE_NOT_EXISTS" not found in file ', Exception::SUB_TEMPLATE_NOT_FOUND),
            static function () use ($obj) {
                $obj->showSub('TEMPLATE_NOT_EXISTS', true);
            }
        );
    }

    public function testShowSubTrue(): void
    {
        $obj = new Template();
        $obj->set($this->content);
        $obj->showSub('TEMPLATE', true);
        $expect = '<body>[%VALUE%]content of subtemplate</body>';
        $this->tester->assertEquals($expect, $obj->get());
    }

    public function testStripSub(): void
    {
        $obj = new Template();
        $obj->set($this->content);
        $obj->stripSub('TEMPLATE');
        $expect = '<body>[%VALUE%]content of subtemplate</body>';
        $this->tester->assertEquals($expect, $obj->get());
    }

    public function testStripSubTagNotExists(): void
    {
        $obj = new Template();

        $this->tester->expectThrowable(
            new Exception('sub template "TEMPLATE_NOT_EXISTS" not found in file ', Exception::SUB_TEMPLATE_NOT_FOUND),
            static function () use ($obj) {
                $obj->stripSub('TEMPLATE_NOT_EXISTS');
            }
        );
    }

    protected function _after()
    {
        if (is_file($this->file)) {
            unlink($this->file);
        }
        parent::_after();
    }

    protected function _before()
    {
        parent::_before();
        $this->file = tempnam(sys_get_temp_dir(), 'tmp');
        $this->content = '<body>[%VALUE%]<!-- <SUB_TEMPLATE> -->content of subtemplate<!-- </SUB_TEMPLATE> --></body>';

        if (is_file($this->file)) {
            unlink($this->file);
        }
        file_put_contents($this->file, $this->content);
    }
}
